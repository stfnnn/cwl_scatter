FROM openjdk:8
COPY FindMaximumBill.java /usr/src/cwl2/
COPY FindMaxOfMax.java /usr/src/cwl2/
WORKDIR /usr/src/cwl2/
RUN javac FindMaximumBill.java FindMaxOfMax.java
