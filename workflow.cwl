#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}

inputs:
  files:
    type:
      type: array
      items: File
    inputBinding:
      position: 1
  callPrice:
    type: string
    inputBinding:
      position: 2
  smsPrice:
    type: string
    inputBinding:
      position: 3
outputs:
  finalOutput:
    type: File
    outputSource: maxBillOverall/resultFinal

steps:
  maxBillPerFile:
    run:
      class: CommandLineTool
      hints:
        DockerRequirement:
          dockerPull: stfnnn/cwl_scatter_domaci:final
      baseCommand: [java, -cp, /usr/src/cwl2, FindMaximumBill]

      requirements:
        InitialWorkDirRequirement:
          listing:
            - $(inputs.fls)
      inputs:
        fls:
          type: File
          inputBinding:
            position: 1
            valueFrom: $(self.basename)
        call:
          type: string
          inputBinding:
            position: 2
        sms:
          type: string
          inputBinding:
            position: 3
      outputs:
        result:
          type: File
          outputBinding:
            glob: "*result.txt"
    scatter: fls
    in:
      fls: files
      call: callPrice
      sms: smsPrice
    out:
      [result]

  catToResultFile:
    run:
      class: CommandLineTool
      baseCommand: cat
      inputs:
        inputFile:
          type: File[]
          inputBinding:
            position: 1
      outputs:
        outputFile:
          type: stdout
      stdout: "result.txt"
    in:
      inputFile: maxBillPerFile/result
    out:
      [outputFile]

  maxBillOverall:
    run:
      class: CommandLineTool
      hints:
        DockerRequirement:
          dockerPull: stfnnn/cwl_scatter_domaci:final
      baseCommand: [java, -cp, /usr/src/cwl2, FindMaxOfMax]
      inputs:
        result:
          type: File
          inputBinding:
            position: 1
      outputs:
        resultFinal:
          type: File
          outputBinding:
            glob: "*.txt"
    in:
      result: catToResultFile/outputFile
    out: [resultFinal]
