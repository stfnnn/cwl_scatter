import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FindMaxOfMax {
	
	private static ArrayList<FinalBill> trafficFinal = new ArrayList<FinalBill>();

	public static void main(String[] args) throws IOException {
		
		String filePath = args[0];

		File file = new File(filePath);
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		String st; 
		while ((st = br.readLine()) != null) {
			String number = st.split(",")[0];
			Float amount = Float.parseFloat(st.split(",")[1]);
			Boolean found = false;
			
			for(FinalBill b: trafficFinal) {
				if(b.getNumber().equals(number)) {
					b.setAmount(b.getAmount() + amount);
					found = true;
				}
			}
			if(!found) {
				FinalBill b = new FinalBill(number, amount);
				trafficFinal.add(b);
			}
		}
			
		br.close();
		
		Collections.sort(trafficFinal, billComparator);
		
		FileOutputStream fop = null;
		File f;
		
		try {

			f = new File("resenje.txt");
			fop = new FileOutputStream(f, true);

			if (!f.exists()) {
				f.createNewFile();
			}
			
			byte[] contentInBytes = String.format("%s, %s\n", trafficFinal.get(0).getNumber(), trafficFinal.get(0).getAmount().toString()).getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

			
			
	}
		
	public static Comparator<FinalBill> billComparator = new Comparator<FinalBill>() {

		public int compare(FinalBill b1, FinalBill b2) {
		   Float amount1 = b1.getAmount();
		   Float amount2 = b2.getAmount();

		   return amount2.compareTo(amount1);
	    }
	};
}

class FinalBill {

	private String number;
	private Float amount;
	
	public FinalBill(String number, Float amount) {
		this.number = number;
		this.amount = amount;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}
}
