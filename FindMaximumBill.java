import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FindMaximumBill {
	
	private static ArrayList<Bill> traffic = new ArrayList<Bill>();

	public static void main(String[] args) throws Exception {
		
		String filePath = args[0];
		Float callPrice = Float.parseFloat(args[1]);
		Float smsPrice = Float.parseFloat(args[2]);
		
		File file = new File(filePath);
		BufferedReader br = new BufferedReader(new FileReader(file)); 

		String st; 
		while ((st = br.readLine()) != null) {
			String number = st.split(",")[0];
			String type = st.split(",")[1];
			Float amount = Float.parseFloat(st.split(",")[2]);
			Boolean found = false;
			
			for(Bill b: traffic) {
				if(b.getNumber().equals(number)) {
					if(type.equals("poziv"))
						b.setAmount(b.getAmount() + callPrice * amount);
					else if (type.equals("sms"))
						 b.setAmount(b.getAmount() + smsPrice * amount);
					else
						throw new Exception("Unknown traffic type!");
					found = true;
				}
			}
			if(!found) {
				Bill b = new Bill(number, amount);
				if(type.equals("poziv"))
					b.setAmount(callPrice * amount);
				else if (type.equals("sms"))
					 b.setAmount(smsPrice * amount);
				else
					throw new Exception("Unknown traffic type!");
				traffic.add(b);
			}
		}
		br.close();
		
		Collections.sort(traffic, billComparator);
		
		FileOutputStream fop = null;
		File f;
		
		try {

			f = new File(filePath.split("\\.")[0] + "result.txt");
			fop = new FileOutputStream(f, true);

			if (!f.exists()) {
				f.createNewFile();
			}
			
			byte[] contentInBytes = String.format("%s, %s\n", traffic.get(0).getNumber(), traffic.get(0).getAmount().toString()).getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	
	public static Comparator<Bill> billComparator = new Comparator<Bill>() {

		public int compare(Bill b1, Bill b2) {
		   Float amount1 = b1.getAmount();
		   Float amount2 = b2.getAmount();

		   return amount2.compareTo(amount1);
	    }
	};
}

class Bill {

	private String number;
	private Float amount;
	
	public Bill(String number, Float amount) {
		this.number = number;
		this.amount = amount;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}
}
